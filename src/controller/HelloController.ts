import {Get, Query, Route, Tags} from "tsoa";
import { BasicResponse } from "./types";
import { IHelloController } from "./interfaces";
import { LogSuccess } from "../utils/logger";

@Route("/api/hello")
@Tags("HelloController")

export class HelloController implements IHelloController {
    /**
     * Endpoint to return a hello {name} message
     * @param { string | undefined } name Name of the person to say hello to
     * @returns {BasicResponse} Promise of BasicResponse
     */
    @Get("/")
    public async getMessage(@Query()name?: string): Promise<BasicResponse> {
        LogSuccess('[/api/hello] Get Request')

        return {
            message: `Hello, ${name || "World!"}`
        }

    }
    
}