import { Delete, Get, Query, Route, Tags } from "tsoa"
import { IUserController } from "./interfaces"
import { LogSuccess, LogError, LogWarning } from "../utils/logger"

// ORM  - Users Collection
import { deleteUserById, getAllUsers, getUserById, createUser, updateUserByID } from "../domain/orm/User.orm"


@Route("/api/users")
@Tags("UserController")
export class UserController implements IUserController {
        
        /**
        * Endpoint to return all users or a specific user
        * @param {string} id Id of the user (optional) 
        * @return All users or a specific user
        */
        @Get("/")
       public async getUsers(@Query()id?: string): Promise<any> {

       let response: any = '';

        if(id) {
                LogSuccess(`[/api/users] Get User by ID: ${id}`)
                response = await getUserById(id)
        } else {
                LogSuccess('[/api/users] Get All Users')
                response = await getAllUsers()
        }
             return response
    }
        /**
        * Endpoint to deleten all users or a specific user
        * @param {string} id Id of the user (optional) 
        * @return message: User deleted successfully
        */
        @Delete("/")
        public async deleteUser(@Query()id?: string): Promise<any> {

        let response: any = ''

        if(id) {
                LogSuccess(`[/api/users] Delete User by ID: ${id}`)
                await deleteUserById(id).then((r) => {
                        response = {
                                message: `User ID: ${id} deleted successfully`
                        }
                })
                
        } else {
                LogWarning('[/api/users] Delete Users WITHOUT ID')
                response = {
                        message: 'Delete Request'
                }
        }
            
                return response
    }

       // @Post("/")
        public async createUser(user: any): Promise<any> {
                
                let response: any = ''

                await createUser(user).then((r) => {
                        LogSuccess(`[/api/users] Create User: ${user.name}`)
                        response = {
                                message: `User created successfully`
                        }
                })

                return response

    }

        // @Put("/")
        public async updateUser(@Query()id: string, user: any): Promise<any> {
                        
                        let response: any = ''

                        if(id) {
                        LogSuccess(`[/api/users] Update User: ${user.name}`)
                        await updateUserByID(id, user).then((r) => {
                                response = {
                                        message: `User updated successfully`
                                }
                        }) 

                        } else {
                                LogWarning('[/api/users] Update Users WITHOUT ID')
                                response = {
                                        message: 'Update Request'
                        }
         }
                        
        return response
       
        }
}


