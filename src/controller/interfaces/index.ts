import { BasicResponse, ErrorResponse } from "../types"

export interface IHelloController {
    getMessage(name?:string): Promise<BasicResponse>
}

export interface IUserController {
    
    // Read all | Find by id
    getUsers(id?:string): Promise<any>
    // Delete by id
    deleteUser(id?:string): Promise<any>
    // Create
    createUser(user: any): Promise<any>
    // Update
    updateUser(id: string, user: any): Promise<any>

}