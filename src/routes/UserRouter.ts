import express, { Request, Response } from "express";
import { UserController } from "../controller/UserController";
import { LogInfo } from "../utils/logger";
import { updateUserByID } from './../domain/orm/User.orm';

// Router from express
let usersRouter = express.Router()

// http://localhost:8000/api/users?id=1
usersRouter.route('/')
    // GET:
    .get(async (req: Request, res: Response) => {
        // Get the query params
        let id: any = req?.query?.id
        LogInfo(`Query Param: ${id}`)
        // Controller Instance to excute method
        const controller: UserController = new UserController()
        // Obtain Response
        const response: any = await controller.getUsers()
        // Send to the client the response
        return res.send(response)
    })

    // DELETE:
    .delete(async (req: Request, res: Response) => {
       // Get the query params
       let id: any = req?.query?.id
       LogInfo(`Query Param: ${id}`)
       // Controller Instance to excute method
       const controller: UserController = new UserController()
       // Obtain Response
       const response: any = await controller.deleteUser(id)
       // Send to the client the response
       return res.send(response)
    })

    // POST:
    .post(async (req: Request, res: Response) => {
        
        let name: any = req?.body?.name
        let age: any = req?.body?.age
        let email: any = req?.body?.email
        
         // Controller Instance to excute method
        const controller: UserController = new UserController()

        let user = {
            name: name || "default",
            age: age || "default",
            email: email || 0
        }


        // Obtain Response
        const response: any = await controller.createUser(user)
        // Send to the client the response
        return res.send(response)
    })
    .put(async (req: Request, res: Response) => {
        // Get the query params))
        let id: any = req?.query?.id
        let name: any = req?.body?.name
        let age: any = req?.body?.age
        let email: any = req?.body?.email
        LogInfo(`Query Param: ${id}`)
        LogInfo(`Body Param: ${name}`)
        LogInfo(`Body Param: ${age}`)
        LogInfo(`Body Param: ${email}`)
        // Controller Instance to excute method
        const controller: UserController = new UserController()
        // Obtain Response
        const response: any = await controller.updateUser(id, {
            name: "default",
            age: "default",
            email: 0
        })
        // Send to the client the response
        return res.send(response)
    })

// Export Hello Router
export default usersRouter
