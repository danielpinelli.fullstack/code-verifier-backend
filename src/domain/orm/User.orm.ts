import { userEntity } from '../entities/User.entity'

import { LogSuccess, LogError } from '../../utils/logger'

// CRUD

/**
 * Get all users
 */

export const getAllUsers = async (): Promise<any[] | undefined> => {
    try {
        let userModel = userEntity()

        //Search all users
        return await userModel.find({ isDeleted: false })
    } catch (error) {
        LogError(`[ORM ERROR]: Getting all users failed. Error: ${error}`)
    }
}

// Get user by id
export const getUserById = async (id: string): Promise<any | undefined> => {

    try {
        let userModel = userEntity()

        //Search user by id
        return await userModel.findById(id)
    } catch (error) {
        LogError(`[ORM ERROR]: Getting user by id failed. Error: ${error}`)
    }
}

// DELETE user by id
export const deleteUserById = async (id: string): Promise<any | undefined> => {
    
        try {
            let userModel = userEntity()
    
            //Search user by id
            return await userModel.deleteOne({_id: id})
        } catch (error) {
            LogError(`[ORM ERROR]: Deleting user by id failed. Error: ${error}`)
        }
}

// Create user
export const createUser = async (user: any): Promise<any | undefined> => {
    try {
        let userModel = userEntity()

        //Create user
        return await userModel.create(user)
    } catch (error) {
        LogError(`[ORM ERROR]: Creating user failed. Error: ${error}`)
    }
}

// Update user
export const updateUserByID = async (id: string, user: any): Promise<any | undefined> => {
    try {
        let userModel = userEntity()

        //Update user
        return await userModel.findByIdAndUpdate(id, user)
    } catch (error) {
        LogError(`[ORM ERROR]: Updating user failed. Error: ${error}`)
    }
}