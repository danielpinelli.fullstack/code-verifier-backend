import dotenv from 'dotenv'
import server from './src/server'
import { LogSuccess, LogError } from './src/utils/logger'

// .env
dotenv.config()

const port = process.env.PORT || 8000

//  Execute Server
server.listen(port, () => {
    LogSuccess(`[SERVER ON]: Running at http://localhost:${port}`)
})

//  Control SERVER ERRORS
server.on('error', (error) => {
    LogError(`[SERVER ERROR]: ${error}`)
})
